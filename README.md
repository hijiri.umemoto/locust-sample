# Locustで負荷試験（サンプル）

locustの負荷試験を実施するサンプルです。

開発環境としてはdevcontainerを使います。

## サンプルAPI立ち上げ

```bash
uvicorn app.main:app --host=0.0.0.0 --reload
```

## 負荷テスト環境立ち上げ

### 一定量負荷テスト

単なる負荷テスト

```bash
locust -f test_load.py
```

### ランディング風テスト

ランディングテストのような形式のテスト。

```bash
locust -f test_running.py
```
