from locust import HttpUser, task
from my_locust.common.user import SampleUser
import random


class SampleApi(SampleUser, HttpUser):
    user_token = ""  # ユーザ用トークン
    api_key = ""  # IoTなど用のAPIキー

    @task(1)
    def insect(self):
        self.client.get('/insect')

    @task(1)
    def user_type_task(self):
        self.set_running_mode(True)
        if self.user_active() is True:
            self.user_login(
                email='test@test.com',
                pwd="password"
            )
            self.client.get('/user/me', headers=self.user_header())
            self.client.put(
                '/insect/buy',
                headers=self.user_header(),
                json={"count": random.randint(1, 100)}
            )

    def user_login(self, email: str, pwd: str):
        '''ログイン処理
        '''
        response = self.client.post(
            '/user/login',
            headers={
                "Content-Type": "application/json"
            },
            json={
                "email": email,
                "password": pwd
            }
        )

        self.user_token = response.json()["token"]

    def user_header(self):
        return {
            "Authorization": f'Bearer {self.user_token}',
            "Content-Type": "application/json"
        }
