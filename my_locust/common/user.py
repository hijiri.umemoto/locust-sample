from datetime import datetime
import random

# アクセスが集中する割合
access_rate = {
    0: 0.05,
    1: 0.05,
    2: 0.05,
    3: 0.05,
    4: 0.05,
    5: 0.05,
    6: 0.2,
    7: 0.4,
    8: 0.5,
    9: 0.65,
    10: 0.75,
    11: 0.9,
    12: 1.0,
    13: 0.9,
    14: 0.75,
    15: 0.65,
    16: 0.5,
    17: 0.4,
    18: 0.2,
    19: 0.05,
    20: 0.05,
    21: 0.05,
    22: 0.05,
    23: 0.05
}


class SampleUser:
    running_mode = False

    def set_running_mode(self, mode: bool):
        """ユーザのアクティブな時間帯
        Args:
            mode: ランディングテストモードにするか
        """
        self.running_mode = mode

    def user_active(self):
        """アクティブな時間帯をベースにリクエストがするかと検証する
        """
        if self.running_mode is False:
            return True

        # リクエストするかを計算する
        hour = datetime.now().hour
        rate = access_rate[hour]

        if random.random() <= rate:
            return True

        return False
