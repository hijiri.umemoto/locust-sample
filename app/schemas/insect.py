from pydantic import BaseModel, Field


class BuyRequest(BaseModel):
    count: int = Field(None, example=1)
