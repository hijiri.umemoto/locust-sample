from fastapi import FastAPI

from app.routes.insect import insect
from app.routes.user import user

app = FastAPI()

app.include_router(insect)
app.include_router(user)
