
from fastapi import APIRouter, Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from datetime import datetime
import time
from app.schemas.insect import BuyRequest

insect = APIRouter(
    prefix='/insect'
)
security = HTTPBearer()


@insect.get("/")
async def list():
    # スリープという形で負荷を表現する
    # time.sleep(random.random() / 1000)

    main_data = [
        {"id": 1, "name": "grasshopper"},
        {"id": 2, "name": "caelifera"},
        {"id": 3, "name": "locust"},
        {"id": 4, "name": "katydid"},
        {"id": 5, "name": "oedaleus"},
        {"id": 6, "name": "aiolopus"},
        {"id": 7, "name": "eusphingonotus"},
        {"id": 8, "name": "trilophidia"},
        {"id": 9, "name": "buforania"},
        {"id": 10, "name": "chorthippus"},
        {"id": 11, "name": "oxyinae"},
        {"id": 12, "name": "cyrtacanthacridinae"},
        {"id": 13, "name": "traulia"},
        {"id": 14, "name": "parapodisma"},
        {"id": 15, "name": "parapodisma"},
        {"id": 16, "name": "sinopodisma"},
        {"id": 17, "name": "podisma"},
        {"id": 18, "name": "ognevia"},
        {"id": 19, "name": "pyrgomorphidae"},
        {"id": 20, "name": "euparatettix"},
        {"id": 21, "name": "eucriotettx"},
        {"id": 22, "name": "criotettix"},
        {"id": 23, "name": "formosatettix"},
        {"id": 24, "name": "tetrix"},
        {"id": 25, "name": "gampsocleis"},
        {"id": 26, "name": "tettigonia"},
        {"id": 27, "name": "eobiana"},
        {"id": 28, "name": "euconocephalus"},
        {"id": 29, "name": "conocephalus"},
        {"id": 30, "name": "conocephalinae"},
        {"id": 31, "name": "mecopodinae"},
        {"id": 32, "name": "hexacentrus"},
        {"id": 33, "name": "ducetia"},
        {"id": 34, "name": "phaneroptera"},
        {"id": 35, "name": "shirakisotima"},
        {"id": 36, "name": "holochlora"},
        {"id": 37, "name": "phaulula"},
        {"id": 38, "name": "homoeoxipha"},
        {"id": 39, "name": "rigonidium"},
        {"id": 40, "name": "dianemobius"},
        {"id": 41, "name": "polionemobius"},
        {"id": 42, "name": "pteronemobius"},
        {"id": 43, "name": "metiochodes"},
        {"id": 44, "name": "natula"},
        {"id": 45, "name": "mogoplistidae"},
        {"id": 46, "name": "gryllotalpidae"},
        {"id": 47, "name": "gryllidae"},
        {"id": 48, "name": "myrmecophilidae"},
        {"id": 49, "name": "gryllacrididae"},
        {"id": 50, "name": "rhaphidophoridae"},
        {"id": 51, "name": "一号"},
        {"id": 52, "name": "二号"},
        {"id": 53, "name": "V3"},
        {"id": 54, "name": "X"},
        {"id": 55, "name": "アマゾン"},
        {"id": 56, "name": "ストロンガー"},
        {"id": 57, "name": "スーパー1"},
        {"id": 58, "name": "BLACK"},
        {"id": 59, "name": "BLACKRX"}
    ]

    return main_data[0:(datetime.now().second + 1)]


@insect.put("/buy")
async def buy(
    body: BuyRequest,
    token: HTTPAuthorizationCredentials = Depends(security)
):
    # 買う件数と時間帯で少し遅延させる
    time.sleep(body.count * abs(12 - datetime.now().hour) / 1000)

    return {
        'status': 'OK'
    }
