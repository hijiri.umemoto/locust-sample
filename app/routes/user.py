
from fastapi import APIRouter, Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

user = APIRouter(
    prefix='/user'
)
security = HTTPBearer()


@user.post("/login")
async def login():
    return {"token": "token"}


@user.get("/me")
async def me(
    token: HTTPAuthorizationCredentials = Depends(security)
):
    return {
        "id": 1,
        "name": "test_user"
    }
