FROM python:3.10-bullseye

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
ENV POETRY_HOME=/opt/poetry
ENV GIT_VERSION=2.43.0
ENV CURL_VERSION=7.88.1

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        gettext=0.21-4 \
        libopencv-dev=4.5.1+dfsg-5 \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# 必要なパケージをダウンロードして解凍する
RUN curl -sL https://github.com/git/git/archive/v${GIT_VERSION}.tar.gz > v${GIT_VERSION}.tar.gz \
    && tar -zxf v${GIT_VERSION}.tar.gz \
    && rm -rf v${GIT_VERSION}.tar.gz \
    && curl -sSL https://curl.se/download/curl-${CURL_VERSION}.tar.gz > curl-${CURL_VERSION}.tar.gz \
    && tar -zxf curl-${CURL_VERSION}.tar.gz \
    && rm -rf curl-${CURL_VERSION}.tar.gz 

# git最新化
WORKDIR /git-${GIT_VERSION}
RUN apt-get remove -y git \
    && make prefix=/usr/local all \
    && make prefix=/usr/local install

# curl最新化
WORKDIR /curl-${CURL_VERSION}
RUN apt-get remove -y curl \
    && ./configure --with-openssl --enable-libcurl-option \
    && make prefix=/usr/local all \
    && make prefix=/usr/local install \
    && cp /usr/local/lib/libcurl.so.4 /usr/lib/x86_64-linux-gnu/libcurl.so.4

# poetryのインストール
RUN curl -L https://install.python-poetry.org | python \
    && ln -s /opt/poetry/bin/poetry /usr/local/bin/poetry \
    && poetry config virtualenvs.create false
